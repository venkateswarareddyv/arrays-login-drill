let givenData=require('./2-arrays-logins.cjs');

function splitIpAdress(data){
    let splitedIpAdress=data.map((object)=>{
        let splitedIp=object.ip_address.split('.')
        object.ip_address=splitedIp;
        
        return object
    })
    return splitedIpAdress
}

let result=splitIpAdress(givenData);

console.log(result);