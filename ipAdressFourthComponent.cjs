let givenData=require('./2-arrays-logins.cjs');

function ipAdressFourthComponent(data){
    let sum=data.reduce((initialValue,object)=>{
        let splitedIp=object.ip_address.split('.')
        initialValue= initialValue+parseInt(splitedIp[3]);

        return initialValue
    },0)
    return sum;
}

let result=ipAdressFourthComponent(givenData);

console.log(result);