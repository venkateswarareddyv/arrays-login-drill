let givenData=require('./2-arrays-logins.cjs');

function ipAdressSecondComponent(data){
    let sum=data.reduce((initialValue,object)=>{
        let splitedIp=object.ip_address.split('.')
        initialValue= initialValue+parseInt(splitedIp[1]);

        return initialValue
    },0)
    return sum;
}

let result=ipAdressSecondComponent(givenData);

console.log(result);