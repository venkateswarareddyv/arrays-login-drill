let givenData=require('./2-arrays-logins.cjs');

function toGetFullName(data){
    let fullName=data.map((object)=>{
        let fullNames=object.first_name+" "+object.last_name;
        object.full_name=fullNames
        
        return object
    })
    return fullName;
}

let result=toGetFullName(givenData);

console.log(result);